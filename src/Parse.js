
'use strict';

import querystring from 'querystring';
import _ from 'underscore';

import ExtendableError from './Error';

/**
 * 
 * @class Parse
 * @constructor
 * @param {Object} data - A data from request.
 */
export default class Parse {

    constructor(data) {
        this._data = data;
    }

    /**
     * Parse JSON document.
     * @return {Object} data.
     * @return {Object} error.
     */
    parseJSON() {
        if (typeof this._data == 'object') {
            return this._data;
        }

        try {
            let data = JSON.parse(this._data);
            if (_.isEmpty(data) === true) {
                let error = new ExtendableError(
                    ExtendableError.OBJECT_NOT_FOUND,
                    'Object of the requested data is empty or does not exist.'
                );

                return error;
            }

            return data;
        } catch (e) {
            let error = new ExtendableError(
                ExtendableError.INVALID_JSON,
                'Invalid JSON.'
            );

            return error;
        }

    }

    /**
     * Parse querystring.
     * @return {String}
     * @return {Object} error.
     */
    parseQuerysting() {
        try {
            return querystring.parse(this._data);
        } catch (e) {
            let error = new ExtendableError(
                ExtendableError.INVALID_URL,
                'Invalid URL.'
            );

            return error;
        }
    }
}