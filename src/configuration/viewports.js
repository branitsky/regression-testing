viewports = [
  // {
  //   "name": "smartphone_portrait",
  //   "viewport": {"width": 320, "height": 480}
  // },
  {
    "name": "desktop_standard_sxga",
    "viewport": {"width": 1280, "height": 1024}
  },
  // {
  //   "name": "tablet-portrait",
  //   "viewport": {"width": 568, "height": 1024}
  // },
  // {
  //   "name": "tablet-landscape",
  //   "viewport": {"width": 1024, "height": 768}
  // },
  // {
  //   "name": "desktop-standard-sxga-plus",
  //   "viewport": {"width": 1400, "height": 1050}
  // },
  // {
  //   "name": "desktop-standard-uxga",
  //   "viewport": {"width": 1600, "height": 1200}
  // },
  // {
  //   "name": "desktop-standard-wuxga",
  //   "viewport": {"width": 1920, "height": 1200}
  // },
];

module.exports = viewports;