'use strict';

import cluster from 'cluster';
import http from 'http';
import os from 'os';

import HandleRequest from './HandleRequest';
import defaults from './configuration/defaults';

if (!global._babelPolyfill) {
    require('babel-polyfill');
}

let host = defaults.host;
let port = defaults.port;

function startServer() {
    let server = http.createServer();
    server.on('request', HandleRequest);

    server.listen(port, host, () => {
        console.log(`[${process.pid}] Server is running on ${host}:${port} \nPress CTRL+C to shutdown.`);
    });

    server.on('error', (error) => {
        Error(error, server);
    });

    let shutdown = function () {
        console.log(`Termination signal received. Shutting down...`);
        server.close(() => {
            process.exit(0);
        });
    }

    process.on('SIGTERM', shutdown);
    process.on('SIGINT', shutdown);
}

function Error(error, server) {
    if (error.code === 'EADDRINUSE') {
        console.error(`Port ${port} is already in use. Retrying...`);
        setTimeout(() => {
            server.close();
            server.listen(port, host);
        }, 5000);
    } else {
        throw error;
    }
}
if (process.env.CLUSTERED) {
    const numCPUs = process.env.CLUSTER_CORES || os.cpus().length;
    if (cluster.isMaster) {
        for (let i = 0; i < numCPUs; i++) {
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            if (signal) {
                console.log(`Signal received: ${signal}`);
            }

            console.log(`Worker ${worker.process.pid} died. Restarting...`);
            cluster.fork();
        });
    }

    if (cluster.isWorker) {
        startServer();
    }
} else {
    startServer();
}