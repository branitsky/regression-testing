'use strict';

import path from 'path';
import fs from 'fs';

// import ExtendableError from './ExtendableError';

/**
 * 
 * @class FS
 * @constructor
 * @param {String} directory
 */
export default class FS {

    constructor(directory) {
        this._directory = directory;
    }

    create() {
        // return new Promise((resolve, reject) => {
            fs.access(this._directory, (error) => {
                // console.log(error ? 'No access!' : 'Can read/write');
                if (error) {
                    fs.mkdirSync(this._directory);
                    console.log(`Directory ${this._directory} has been created.`);
                }
                return this._directory;
                // resolve(this._directory);
            });
        // });
    }

    read() {
        return new Promise((resolve, reject) => {
            fs.readdir(this._directory, (error, files) => {
                if (error) reject(error);
                resolve(files);
            });
        });
    }

    deleteUnusedFiles() {
        return this.read().then((files) => {
            for (let file of files) {
                let stats = fs.statSync(path.join(this._directory, file));
                return [file, stats];
            }
        }).then((values) => {
            let file = values[0];
            let stats = values[1];

            let now = new Date().getTime();
            let createdAt = new Date(stats.ctime).getTime() + 86400000;
            if (now > createdAt) {
                fs.unlink(path.join(this._directory, file), (error) => {
                    if (error) throw error;
                    
                    console.log(`Deleted ${file}`);
                });
            }

        }).catch((error) => {

        });
    }
}