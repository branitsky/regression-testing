'use strict';

var casper = require('casper').create({
  logLevel: 'warning',
  pageSettings: { javascriptEnabled: true }
});

var fs = require('fs');
var utils = require('utils');

var viewports = require('./configuration/viewports');

utils.dump(casper.cli.options);
var originalUrl = casper.cli.get("original");
var reformedUrl = casper.cli.get("reformed");
var directory = casper.cli.get("directory");
var screenshotUrls = [originalUrl, reformedUrl];
var screenshotNames = ['original', 'reformed'];

casper.on('resource.requested', function (requestData, request) {
  if (requestData.url.indexOf('server.cpmstar.com') > -1) {
    console.log('Skipped: ' + requestData.url);
    request.abort();
  }
});

var i = -1;

casper.on('resource.error', function (error) {
  casper.log('Resource load error: ' + error, 'warning');
});

casper.on('error', function (error) {
  this.die('Casperjs has errored: ' + error);
});

casper.start().eachThen(screenshotUrls, function (response) {

  this.each(viewports, function (casper, vp) {

    this.then(function () {
      this.viewport(vp.viewport.width, vp.viewport.height);
    });

    this.thenOpen(response.data, function () {
      this.wait(2000);
    });

    this.then(function () {
      var pageWidth = this.evaluate(function () {
        return document.body.clientWidth;
      });
      var pageHeight = this.evaluate(function () {
        return document.body.clientHeight;
      });

      // this.wait(2000);
      this.viewport(pageWidth, pageHeight).then(function () {

        this.capture(directory + '/' + screenshotNames[i] + '_' + vp.name + '.png', undefined, {
          quality: 100
        });

      });
    });
  });

  ++i;
});

casper.run(function () {
  console.log('DONE');
  casper.exit();
});