'use strict';

import _ from 'underscore';

import Parse from './Parse';
import ExtendableError from './Error';

/**
 *
 * @class Router
 * @constructor
 * @param {Object} request - A client request.
 * @param {Object} data - A data from the request. 
 */
export default class Router {
    
    constructor(request, data) {
        this._request = request;
        this._data = data;
    }

    route() {
        let request = this._request;
        let method = request.method.toLowerCase();

        if (method === 'post') {
            return this.handlePost();
        }

        let error = new ExtendableError(
            ExtendableError.METHOD_NOT_ALLOWED,
            'Method not allowed.'
        );

        return error;
    }

    handlePost() {
        let request = this._request;
        let header = request.headers['content-type'];

        if (!header) {
            let error = new ExtendableError(
                ExtendableError.MISSING_CONTENT_TYPE,
                'Content-type header are missing.'
            );

            return error;
        } else if (header.indexOf('application/json') !== 0) {
            let error = new ExtendableError(
                ExtendableError.UNSUPPORTED_MEDIA_TYPE,
                'Content-type must be application/json.'
            );

            return error;
        }

        let parse = new Parse(this._data);
        let object = parse.parseJSON();

        return object;
    }
}