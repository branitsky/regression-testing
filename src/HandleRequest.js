'use strict';

import Middlewares from './Middlewares'

export default function HandleRequest(request, response) {
    let body = '';
    request.on('data', (chunk) => {
        body += chunk;
    });

    request.on('end', () => {
        Middlewares(request, response, body);
    });
}