'use strict';

/**
 * Constructs a new object with the given name and message.
 * 
 * @class ExtendableError
 * @constructor
 * @param {Number} code - An error code.
 * @param {String} message - A detailed description of the error.
 * 
 */
export default class ExtendableError extends Error {
    
    constructor(code, message) {
        super(message);

        Object.defineProperty(this, 'message', {
            configurable: true,
            enumerable : false,
            value : message,
            writable : true,
        });

        Object.defineProperty(this, 'code', {
            configurable: true,
            enumerable : false,
            value : code,
            writable : true,
        });

        Object.defineProperty(this, 'name', {
            configurable: true,
            enumerable : false,
            value : this.constructor.name,
            writable : true,
        });

        Error.captureStackTrace(this, this.constructor);

        Object.defineProperty(this, 'stack', {
            configurable: true,
            enumerable : false,
            value : (new Error(message)).stack,
            writable : true,
        });
    }

    /**
     * Sends response with the specified error.
     * 
     * @method send
     * @static
     * @param {Object} error - An object that contains error code and message.
     * @param {Object} response - A response to client.
     */
    static send(error, response) {
        console.error(error.stack);

        response.writeHead(error.code, { "Content-Type": "application/json" });
        response.end(JSON.stringify({ message: error.message }));
    }
}

/**
 * Error code indicates that the server has encountered a situation it doesn't know how to handle.
 * @property INTERNAL_SERVER_ERROR
 * @static
 */
ExtendableError.INTERNAL_SERVER_ERROR = 500;

/**
 * Error code indicates that the request method cannot be used.
 * @property METHOD_NOT_ALLOWED
 * @static
 */
ExtendableError.METHOD_NOT_ALLOWED = 405;

/**
 * Error code indicating that content-type header are missing.
 * @property MISSING_CONTENT_TYPE
 * @static
 */
ExtendableError.MISSING_CONTENT_TYPE = 415;

/**
 * Error code indicates that the media type of the requested data is not supported.
 * @property UNSUPPORTED_MEDIA_TYPE
 * @static
 */
ExtendableError.UNSUPPORTED_MEDIA_TYPE = 415;

/**
 * Error code indicating that connection failed.
 * @property CONNECTION_FAILED
 * @static
 */
ExtendableError.CONNECTION_FAILED = 404;

/**
 * Error code indicates that the JSON object of the requested data is empty or does not exist.
 * @property OBJECT_NOT_FOUND
 * @static
 */
ExtendableError.OBJECT_NOT_FOUND = 400;

/**
 * Error indicating that the object contain more than one {key: value} pair.
 * @property OBJECT_TOO_LARGE
 * @static
 */
ExtendableError.OBJECT_TOO_LARGE = 400;

/**
 * Error code indicating that JSON document is badly formed.
 * @property INVALID_JSON
 * @static
 */
ExtendableError.INVALID_JSON = 400;

/**
 * Error code indicating that url has errors.
 * @property INVALID_URL
 * @static
 */
ExtendableError.INVALID_URL = 400;

/**
 * Error code indicating that filename contains invalid characters.
 * @property INVALID_FILE_NAME
 * @static
 */
ExtendableError.INVALID_FILE_NAME = 400;

/**
 * Error code indicating that resource does not exist or the network is failed.
 * @property RESOURCE_DOES_NOT_EXIST
 * @static
 */
ExtendableError.RESOURCE_DOES_NOT_EXIST = 404;

/**
 * Error indicating that the file extension is incorrect.
 * @property INCORRECT_FILE_EXTENSION
 * @static
 */
ExtendableError.INCORRECT_FILE_EXTENSION = 400;

/**
 * Error code indicating that something has gone wrong when saving file.
 * @property FILE_SAVE_ERROR
 * @static
 */
ExtendableError.FILE_SAVE_ERROR = 500;

/**
 * Error code indicating that file does not exist or path to file is invalid.
 * @property FILE_NOT_FOUND
 * @static
 */
ExtendableError.FILE_NOT_FOUND = 500;

/**
 * Error code indicating that directory does not exist or path to directory is invalid.
 * @property DIRECTORY_NOT_FOUND
 * @static
 */
ExtendableError.DIRECTORY_NOT_FOUND = 500;