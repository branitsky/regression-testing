'use strict';

import { exec } from 'child_process';
import crypto from 'crypto';
import path from 'path';
import os from 'os';
import fs from 'fs';

import _ from 'underscore';
import cv from 'opencv';

import ExtendableError from './Error';
import Router from './Router';
import FS from './FS';

import { NAMES } from './configuration/names'

export default function Middlewares(request, response, data) {

    let content = new Router(request, data).route();
    if (content instanceof Error) {
        ExtendableError.send(content, response);
        return content;
    } else if (Object.keys(content).length > 10) {
        let error = new ExtendableError(
            ExtendableError.OBJECT_TOO_LARGE,
            'Object too large.'
        );
        ExtendableError.send(error, response);
        return error;
    }

    let urls = _.values(content);

    // Create a directory with new name.
    let name = crypto.randomBytes(4).toString('hex');
    let directory = path.join(os.tmpdir(), name);
    let folder = new FS(directory).create();

    // Start screen capture.
    let startCapture = `casperjs --ssl-protocol=any ./lib/Capture.js --original=${urls[0].toString()} --reformed=${urls[1].toString()} --directory=${directory}`;
    exec(startCapture, (error, stdout, stderr) => {
        if (error) {
            let error = new ExtendableError(
                ExtendableError.INTERNAL_SERVER_ERROR,
                'Internal server error.'
            );
            ExtendableError.send(error, response);
            return error;
        }
        
        if (stdout.indexOf('DONE') != -1) {
            // Check the difference between the pictures.
            opencv(directory, NAMES, (error, dissimilarity) => {
                if (error) {
                    ExtendableError.send(error, response);
                    return error;
                }

                response.writeHead(200, { 'Content-Type': 'application/json' });
                response.end(JSON.stringify({ Percentage: dissimilarity }));
            });
        }
    });

}

function opencv(directory, names, callback) {
    let results = [];
    for (let name of names) {
        cv.readImage(path.join(directory, name), (err, info) => {
            if (err) {
                // console.error(err);
                let error = new ExtendableError(
                    ExtendableError.INTERNAL_SERVER_ERROR,
                    'Internal server error.'
                );

                callback(error);
            }

            results.push(info);
        });
    }

    cv.ImageSimilarity(results[0], results[1], (err, dissimilarity) => {
        if (err) {
            // console.error(err);
            let error = new ExtendableError(
                ExtendableError.INTERNAL_SERVER_ERROR,
                'Internal server error.'
            );

            callback(error);
        }

        callback(null, dissimilarity);
    });
}