FROM nodesource/jessie:5.9.0

ENV PORT=8086

# Environment
ENV PHANTOM_URL https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
ENV PHANTOM_DIR /usr/local/bin
ENV CASPER_URL https://github.com/n1k0/casperjs/archive/master.tar.gz
ENV CASPER_DIR /usr/local/casperjs

# Commands
RUN set -xe \
    && apt-get update \
    && apt-get install -y bzip2 \
                          curl \
                          libgif-dev \
                          libjpeg-dev \
                          libfontconfig \
                          libicu52 \
                          libsqlite3-0 \
                          libcairo2-dev \
                          python \
    && curl -sSL $PHANTOM_URL | tar xj -C $PHANTOM_DIR --strip 2 --wildcards '*/bin/phantomjs' \
    && chmod +x /usr/local/bin/phantomjs \
    && mkdir -p $CASPER_DIR \
    && curl -sSL $CASPER_URL | tar xz --strip 1 -C $CASPER_DIR \
    && ln -sf $CASPER_DIR/bin/casperjs /usr/local/bin/ \
    && apt-get remove -y bzip2 \
                         curl \
    && rm -rf /var/lib/apt/lists/*

# Create main directory
RUN mkdir -p /usr/src/main
WORKDIR /usr/src/main

# Bundle main source
COPY package.json /usr/src/main

# install dependencies
RUN npm install

COPY . /usr/src/main
RUN npm run prepublish

EXPOSE $PORT

CMD ["npm", "start"]
