'use strict';

const gulp = require('gulp');
const babel = require('gulp-babel');
const spawn = require('child_process').spawn;
var node;

/**
 * $ gulp compile
 * description: compile es6 code to native js.
 */
gulp.task('compile', function () {
  return gulp.src('src/**')
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('lib'));
});

/**
 * $ gulp server
 * description: launch the server. If there's a server already running, kill it.
 */
gulp.task('server', ['compile'], function () {
  if (node) node.kill();
  node = spawn('node', ['index.js'], { stdio: 'inherit' })
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

/**
 * $ gulp
 * description: start the development environment.
 */
gulp.task('build', ['server'], function () {
  gulp.watch([['./index.js', './src/**/*.js']], ['server']);
});

// Clean up if an error goes unhandled.
process.on('exit', function () {
  if (node) node.kill();
});
